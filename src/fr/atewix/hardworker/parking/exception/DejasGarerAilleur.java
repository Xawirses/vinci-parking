package fr.atewix.hardworker.parking.exception;

/**
 * Class DejaGarerAilleur qui est une exception levé lorsque un client, pour un de ses vehicule, essaye de la garer à deux endroits différents
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class DejasGarerAilleur extends Exception {

    public DejasGarerAilleur() {
        super(" Deja Garer Ailleur");
    }
}
