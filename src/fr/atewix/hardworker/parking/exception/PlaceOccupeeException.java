package fr.atewix.hardworker.parking.exception;

/**
 * Class PlaceOccupeeException qui est une exception levé lorsqu'on essaye de garer un véhicule sur une place déjà occupée
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class PlaceOccupeeException extends Exception {

    public PlaceOccupeeException() {
        super("Veuillez choisir une place disponible !");
    }
}
