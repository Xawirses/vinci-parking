package fr.atewix.hardworker.parking.exception;

/**
 * Class PlusAucunePlaceException qui est une exception levé lorsqu'aucune place est disponible
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class PlusAucunePlaceException extends Exception {

    public PlusAucunePlaceException() {
        super("Il n'y a plus de places disponibles en ce moment !");
    }
}
