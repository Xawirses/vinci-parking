package fr.atewix.hardworker.parking.exception;

/**
 * Class ClientDejaCree, qui est une exception levé lorsque la combinaison Nom/Prenom existe déjà parmi les clients
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class ClientDejaCree extends Exception {

    public ClientDejaCree() {
        super("La combinaison Nom / Prenom est déjà utilisée");
    }
}
