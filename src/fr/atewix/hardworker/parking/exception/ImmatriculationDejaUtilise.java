package fr.atewix.hardworker.parking.exception;

/**
 * Class ImmatriculationDejaUtilise qui est une exception levé lorsque on essaye de créer une voiture possédant la même plaque d'immatriculation qu'un vehicule existant
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class ImmatriculationDejaUtilise extends Exception {

    public ImmatriculationDejaUtilise() {
        super("Immatriculation déjà utilisée");
    }
}
