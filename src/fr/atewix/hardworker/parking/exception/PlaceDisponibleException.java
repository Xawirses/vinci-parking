package fr.atewix.hardworker.parking.exception;

/**
 * Class PlaceDisponibleException qui est une exception levé lorsque on essaye d'enlever un vehicule d'une place libre
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class PlaceDisponibleException extends Exception {

    public PlaceDisponibleException() {
        super("Cette place est déjà disponible");
    }
}
