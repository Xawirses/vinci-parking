package fr.atewix.hardworker.parking.exception;

/**
 * Class DonneesNonValides qui est une exception levé lorsque une donnée n'est pas confomre
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class DonneesNonValides extends Exception {

    public DonneesNonValides(String erreur) {
        super("La donnée " + erreur + " n'est pas valide !");
    }
}
