package fr.atewix.hardworker.parking.exception;

/**
 * Class PlaceLibreException qui est une exception levé lorsqu'on essaye d'enlever une reservation d'une place qui est libre
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class PlaceLibreException extends Exception {

    public PlaceLibreException() {
        super("Cette place est déjà libre !");
    }
}
