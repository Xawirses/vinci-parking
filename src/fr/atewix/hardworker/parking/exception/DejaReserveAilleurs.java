package fr.atewix.hardworker.parking.exception;

/**
 * Class DejaReserveAilleurs qui est une exception levé lorsque un client, pour un de ses vehicule, essaye d'effectuer deux reservations
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see java.lang.Exception
 */
public class DejaReserveAilleurs extends Exception {

    public DejaReserveAilleurs() {
        super(" Vous avez déjà reserve une place pour ce vehicule");
    }
}
