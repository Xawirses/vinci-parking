package fr.atewix.hardworker.parking.facture;


import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import org.jetbrains.annotations.NotNull;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Class Facture : permet de fabriquer un objet facture à chaque sortie de vehicule
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class Facture {

    /**
     * Variable statique permettant de donner un numéro unique à chaques factures crées
     */
    private static int NUMERO = 0;

    /**
     * Le numéro de la facture
     */
    private final int numeroFacture;

    /**
     * Vehicule concerné pour la facture
     */
    @NotNull
    private final Vehicule vehiculeconcerne;

    /**
     * Montant total de la facture
     */
    private final double montantfacture;

    /**
     * Date d'arrivée du vehicule au parking
     */
    @NotNull
    private final Calendar datedebut;

    /**
     * Date de sortie du vehicule du parking
     */
    @NotNull
    private final Calendar datefin;

    /**
     * Valeur de la TVA constante
     */
    private final double TVA = 0.196;

    /**
     * Constructeur de la classe facture, qui permet de créer une facture à partir d'un vehicule, d'une date de debut et d'un tarif
     * horaire.
     */
    public Facture(@NotNull Vehicule vehicule, @NotNull Calendar datedebut, int tarifhoraire) {
        ++NUMERO;
        this.numeroFacture = NUMERO;
        this.vehiculeconcerne = vehicule;
        this.datedebut = datedebut;
        this.datefin = new GregorianCalendar();
        this.montantfacture = calculMontantTTC(datedebut, datefin, tarifhoraire, TVA);
    }

    /**
     * Methode permettant de calculer le montant de la facture HT, à partir de la date de debut, date de fin
     * et du tarif horaire.
     *
     * @return Montant de la facture Hors Taxes
     */
    private double calculMontantHT(@NotNull Calendar datedebut, @NotNull Calendar datefin, int tarifhoraire) {
        return ((datefin.get(Calendar.DAY_OF_MONTH) - datedebut.get(Calendar.DAY_OF_MONTH)) * 24 * 3600
                + (datefin.get(Calendar.HOUR_OF_DAY) - datedebut.get(Calendar.HOUR_OF_DAY)) * 3600
                + (datefin.get(Calendar.MINUTE) - datedebut.get(Calendar.MINUTE)) * 60
                + (datefin.get(Calendar.SECOND) - datedebut.get(Calendar.SECOND))) / 3600 * tarifhoraire;
    }

    /**
     * Methode permettant de calculer le montant TTC de la facture
     *
     * @return Montant de la facture Toutes taxes comprises
     */
    private double calculMontantTTC(@NotNull Calendar datedebut, @NotNull Calendar datefin, int tarifhoraire, double TVA) {
        double resultatHT = calculMontantHT(datedebut, datefin, tarifhoraire);
        return (resultatHT * TVA) + resultatHT;
    }

    /**
     * Methode qui permet de placer dans une String toutes les informations utiles à l'objet
     *
     * @return Informations souhaitées de l'objet
     */
    @NotNull
    public String toString() {
        return "Facture NUMERO = " + numeroFacture + "\r\n"
                + vehiculeconcerne.getProprietaire().getNom() + " " + vehiculeconcerne.getProprietaire().getPrenom() + "\r\n"
                + "Vehicule Concerne \r\n"
                + "imatriculation " + vehiculeconcerne.getImmatriculation() + "\r\n"
                + vehiculeconcerne.getMarque() + " " + vehiculeconcerne.getModele() + "\r\n"
                + "Tarif \r\n"
                + "Date d' Arriver:" + "\r\n"
                + datedebut.get(Calendar.DAY_OF_MONTH) + '/' + datedebut.get(Calendar.MONTH) + datedebut.get(Calendar.YEAR) + "\r\n"
                + "A : " + datedebut.get(Calendar.HOUR_OF_DAY) + ':' + datedebut.get(Calendar.MINUTE) + ':' + datedebut.get(Calendar.SECOND) + "\r\n"
                + "Date de depart:" + "\r\n"
                + datefin.get(Calendar.DAY_OF_MONTH) + "/" + datefin.get(Calendar.MONTH) + "/" + datefin.get(Calendar.YEAR) + "\r\n"
                + "A : " + datefin.get(Calendar.HOUR_OF_DAY) + ':' + datefin.get(Calendar.MINUTE) + ':' + datefin.get(Calendar.SECOND) + "\r\n"
                + "TVA=" + TVA * 100 + "%" + "\r\n"
                + "Total :" + arrondirMontant(montantfacture) + " euros";
    }

    /**
     * Methode permettant d'enregistrer une facture dans un fichier externe à l'application
     */
    public void enregistrer() throws Exception {
        @NotNull String nomFacture = "Facture_" + this.numeroFacture + ".txt";
        try (@NotNull FileOutputStream facout = new FileOutputStream(nomFacture)) {
            facout.write(this.toString().getBytes());
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * Methode permettant d'arrondir au dixième près une valeure.
     *
     * @return Valeur arrondie
     */
    private double arrondirMontant(double val) {
        return (Math.floor(val * 100.0)) / 100;
    }
}
