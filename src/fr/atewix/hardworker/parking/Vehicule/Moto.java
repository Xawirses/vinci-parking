package fr.atewix.hardworker.parking.Vehicule;

import fr.atewix.hardworker.parking.business.Client;
import org.jetbrains.annotations.NotNull;

/**
 * Classe motos, permettant la création d'un véhicule de type moto
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
class Moto extends Vehicule {

    /**
     * Constructeur d'un véhicule de type moto
     */
    public Moto(@NotNull String immatriculation, @NotNull Client proprietaire, @NotNull String marque, @NotNull String modele) {
        super(immatriculation, proprietaire, marque, modele, VehiculeType.Moto);
    }
}
