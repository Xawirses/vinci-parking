package fr.atewix.hardworker.parking.Vehicule;

/**
 * @author Jean-Pierre PRUNARET
 * @date 2017-02-17
 */
public enum VehiculeType {
    Moto, Voiture, Camion
}
