package fr.atewix.hardworker.parking.Vehicule;

import fr.atewix.hardworker.parking.business.Client;
import org.jetbrains.annotations.NotNull;

/**
 * Classe abstraite qui définit ce qu'un véhicule doit contenir au minimum
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public abstract class Vehicule {

    @NotNull
    private final String immatriculation;
    @NotNull
    private final Client proprietaire;
    @NotNull
    private final String marque;
    @NotNull
    private final String modele;
    @NotNull
    private final VehiculeType type;

    /**
     * Constructeur minimum d'un véhicule
     */
    Vehicule(@NotNull String immatriculation, @NotNull Client proprietaire, @NotNull String marque, @NotNull String modele, @NotNull VehiculeType type) {
        this.immatriculation = immatriculation;
        this.proprietaire = proprietaire;
        this.marque = marque;
        this.modele = modele;
        this.type = type;
    }

    /**
     * Méthode d'affichage
     */
    public String toString() {
        return String.format("%s %s %s", immatriculation, marque, modele);
    }

    /**
     * Récupérer l'immatriculation
     */
    @NotNull
    public String getImmatriculation() {
        return this.immatriculation;
    }

    /**
     * Récupérer le propriétaire
     */
    @NotNull
    public Client getProprietaire() {
        return this.proprietaire;
    }

    /**
     * Récupérer le type de véhicule
     */
    @NotNull
    public VehiculeType getType() {
        return type;
    }

    /**
     * Récupérer le modéle du véhicule
     */
    @NotNull
    public String getModele() {
        return modele;
    }

    /**
     * Récupérer la marque du véhicule
     */
    @NotNull
    public String getMarque() {
        return marque;
    }
}
