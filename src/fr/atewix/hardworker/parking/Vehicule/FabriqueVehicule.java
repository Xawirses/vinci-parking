package fr.atewix.hardworker.parking.Vehicule;

import fr.atewix.hardworker.parking.business.Client;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Interface de fabrique de véhicules
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public interface FabriqueVehicule {

    /**
     * Methode qui permet de créer un vehicule, en fonction de son type
     *
     * @return Vehicule crée
     */
    @NotNull Vehicule Creer(VehiculeType typeVehicule, String immatriculation, Client proprietaire, String marque, String modele);
}
