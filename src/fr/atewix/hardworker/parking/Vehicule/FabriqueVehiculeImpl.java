package fr.atewix.hardworker.parking.Vehicule;

import fr.atewix.hardworker.parking.business.Client;
import org.jetbrains.annotations.NotNull;

/**
 * Class FabriqueVehiculeImpl, qui implement l'interfacte FabriqueVehicule
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class FabriqueVehiculeImpl implements FabriqueVehicule {

    @NotNull
    @Override
    public Vehicule Creer(@NotNull VehiculeType typeVehicule, @NotNull String immatriculation, @NotNull Client proprietaire, @NotNull String marque, @NotNull String modele) {
        @NotNull final Vehicule vehicule;

        switch (typeVehicule) {
            case Moto:
                vehicule = new Moto(immatriculation, proprietaire, marque, modele);
                break;
            case Voiture:
                vehicule = new Voiture(immatriculation, proprietaire, marque, modele);
                break;
            case Camion:
                vehicule = new Camion(immatriculation, proprietaire, marque, modele);
                break;
            default:
                throw new IllegalStateException();
        }

        return vehicule;
    }
}
