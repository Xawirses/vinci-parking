package fr.atewix.hardworker.parking.Vehicule;

import fr.atewix.hardworker.parking.business.Client;
import org.jetbrains.annotations.NotNull;

/**
 * Classe voiture, permettant la création d'un véhicule de type voiture
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
class Voiture extends Vehicule {
    /**
     * Création d'un véhicule de type voiture
     */
    public Voiture(@NotNull String immatriculation, @NotNull Client proprietaire, @NotNull String marque, @NotNull String modele) {
        super(immatriculation, proprietaire, marque, modele, VehiculeType.Voiture);
    }
}
