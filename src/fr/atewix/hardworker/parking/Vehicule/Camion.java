package fr.atewix.hardworker.parking.Vehicule;

import fr.atewix.hardworker.parking.business.Client;
import org.jetbrains.annotations.NotNull;

/**
 * Classe camion, permettant la création d'un camion
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
class Camion extends Vehicule {

    /**
     * Constructeur d'un véhicule de type camion
     */
    public Camion(@NotNull String immatriculation, @NotNull Client proprietaire, @NotNull String marque, @NotNull String modele) {
        super(immatriculation, proprietaire, marque, modele, VehiculeType.Camion);
    }
}
