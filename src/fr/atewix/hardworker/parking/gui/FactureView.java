package fr.atewix.hardworker.parking.gui;

import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.facture.Facture;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;

class FactureView extends JFrame {
    @NotNull
    private final Facture courant;

    public FactureView() {
        super("Facture");
        @NotNull Parking parking = Parking.getInstance();
        courant = (Facture) parking.getListeFacture().peek();
        //setResizable(false);
        @NotNull JTextArea facturecourant = new JTextArea(courant.toString());
        facturecourant.setEditable(false);
        add(facturecourant, BorderLayout.CENTER);
        add(ImprimerQuiter(), BorderLayout.SOUTH);
        pack();
        setLocation(400, 500);
        setVisible(true);
    }

    @NotNull
    private JPanel ImprimerQuiter() {
        @NotNull JPanel ImprimerQuiter = new JPanel();
        @NotNull JButton Imprimer = new JButton();
        Imprimer.setText("Imprimer");
        Imprimer.setPreferredSize(new Dimension(140, 40));
        Imprimer.addActionListener(e -> {
            try {
                courant.enregistrer();
            } catch (Exception ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }
            @NotNull JFrame information = new JFrame();
            JOptionPane.showMessageDialog(information,
                    "Facture Imprimer",
                    "Information",
                    JOptionPane.INFORMATION_MESSAGE);

            dispose();
        });

        @NotNull JButton Quiter = new JButton();
        Quiter.setText("Quitter");
        Quiter.setPreferredSize(new Dimension(140, 40));
        Quiter.addActionListener(e -> dispose());
        ImprimerQuiter.add(Imprimer, BorderLayout.WEST);
        ImprimerQuiter.add(Quiter, BorderLayout.EAST);
        return ImprimerQuiter;
    }
}
