package fr.atewix.hardworker.parking.gui;

import fr.atewix.hardworker.parking.business.Client;
import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.exception.ClientDejaCree;
import fr.atewix.hardworker.parking.exception.DonneesNonValides;
import fr.atewix.hardworker.parking.gui.ihm.Fenetre;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class AjouterClient
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see fr.atewix.hardworker.parking.gui.ihm.Fenetre
 * @see java.awt.event.ActionListener
 */
public class AjouterClient extends Fenetre implements ActionListener {

    @NotNull
    private final Parking parking = Parking.getInstance();
    @NotNull
    private final JPanel panel = new JPanel();
    @NotNull
    private final JTextField nomtext = new JTextField();
    @NotNull
    private final JTextField prenomtext = new JTextField();
    @NotNull
    private final JTextField adressetext = new JTextField();

    /**
     * Methode pour ajouter un client
     */
    public AjouterClient() {
        super("Ajouter un client", new Dimension(310, 210));
        generateVue();
        add(panel);
        setVisible(true);
    }

    /**
     * Methode qui genere la vue
     */
    private void generateVue() {
        @NotNull JLabel nom = new JLabel("Nom");
        @NotNull JLabel prenom = new JLabel("Prenom");
        @NotNull JLabel adresse = new JLabel("Adresse");

        nomtext.setPreferredSize(new Dimension(300, 20));
        prenomtext.setPreferredSize(new Dimension(300, 20));
        adressetext.setPreferredSize(new Dimension(300, 20));

        panel.add(nom);
        panel.add(nomtext);

        panel.add(prenom);
        panel.add(prenomtext);

        panel.add(adresse);
        panel.add(adressetext);

        @NotNull JButton boutonvalider = new JButton("Valider");
        @NotNull JButton boutonannuler = new JButton("Annuler");

        boutonvalider.addActionListener(this);
        boutonannuler.addActionListener(this);

        panel.add(boutonvalider);
        panel.add(boutonannuler);
    }

    /**
     * méthode qui vérifie les valeurs entrées
     */
    private boolean verifierValeursClient(@NotNull Client client) throws DonneesNonValides, ClientDejaCree {
        @NotNull String nom = client.getNom();
        @NotNull String prenom = client.getPrenom();
        @NotNull String adresse = client.getAdresse();

        if (parking.isNomPrenomExiste(nom, prenom)) {
            throw new ClientDejaCree();
        }
        if (!nom.matches("[a-zA-Z]{2,10}")) {
            throw new DonneesNonValides("nom");
        }
        if (!prenom.matches("[a-zA-Z]{2,10}")) {
            throw new DonneesNonValides("prenom");
        }
        if (!adresse.matches("[a-zA-Z_0-9\\s]{10,32}")) {
            throw new DonneesNonValides("adresse");
        }
        return true;
    }


    /**
     * ActionPerformed pour chaque boutons
     */
    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        if (commande.equals("Valider")) {
            String nom = nomtext.getText();
            String prenom = prenomtext.getText();
            String adresse = adressetext.getText();
            @NotNull Client nouveauClient = new Client(nom, prenom, adresse);
            try {
                if (verifierValeursClient(nouveauClient)) {
                    parking.addClient(nouveauClient);
                    dispose();
                }
            } catch (@NotNull DonneesNonValides | ClientDejaCree ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }
        } else if (commande.equals("Annuler")) {
            dispose();
        }
    }
}
