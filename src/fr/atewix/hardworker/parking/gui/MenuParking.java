package fr.atewix.hardworker.parking.gui;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * Class Menu Pour le menue de l'interfasse graphique
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class MenuParking extends JMenuBar {
    /**
     * constructeur ajoute les composants
     */
    public MenuParking() {
        super();
        add(menuFichier());
        add(menuEditer());
        add(menuClient());
    }

    /**
     * Menu qui permet de quitter ou d'imprimer
     */
    @NotNull
    private JMenu menuFichier() {
        @NotNull JMenu Fichier = new JMenu("Fichier");
        Fichier.add(menuItemImprimerEtat());
        Fichier.add(menuItemQuitter());
        return Fichier;
    }

    /**
     * Menu qui permet de gerer les places(reserver,garer ou enlever un vehicule)
     */
    @NotNull
    private JMenu menuEditer() {
        @NotNull JMenu Editer = new JMenu("Editer");
        Editer.add(menuItemAjouterReservation());
        Editer.add(menuItemEnleverReservation());
        Editer.add(menuItemGarerVehicule());
        Editer.add(menuItemEnleverVehicule());
        return Editer;
    }

    /**
     * Menu permettant de gerer les infos du client
     */
    @NotNull
    private JMenu menuClient() {
        @NotNull JMenu Client = new JMenu("Client");
        Client.add(menuItemAjouterClient());
        Client.add(menuItemAjouterVehicule());
        return Client;
    }

    /**
     * Menu permettant de quitter
     */
    @NotNull
    private JMenuItem menuItemQuitter() {
        @NotNull JMenuItem Quitter = new JMenuItem("Quitter");
        Quitter.addActionListener(e -> System.exit(0));
        return Quitter;
    }

    /**
     * Menu pour imprimer l'etat du parking
     */
    @NotNull
    private JMenuItem menuItemImprimerEtat() {
        @NotNull JMenuItem imprimerEtat = new JMenuItem("Visualiser Etat du Parking");
        imprimerEtat.addActionListener(e -> new EtatParking());
        return imprimerEtat;
    }

    /**
     * Menu permettant d'ajouter les reservations
     */
    @NotNull
    private JMenuItem menuItemAjouterReservation() {
        @NotNull JMenuItem AjouterReservation = new JMenuItem("Ajouter Réservation");
        AjouterReservation.addActionListener(e -> new AjouterReservation());
        return AjouterReservation;
    }

    /**
     * Menu permettant d'enlever les reservations
     */
    @NotNull
    private JMenuItem menuItemEnleverReservation() {
        @NotNull JMenuItem enleverReservation = new JMenuItem("Enlever une Reservation");
        enleverReservation.addActionListener(e -> new EnleverReservation());
        return enleverReservation;
    }

    /**
     * Menu permettant d'ajouter un vehicule
     */
    @NotNull
    private JMenuItem menuItemAjouterVehicule() {
        @NotNull final JMenuItem AjouterVehicule = new JMenuItem("Ajouter Vehicule");
        AjouterVehicule.addActionListener(e -> new AjouterVehicule());
        return AjouterVehicule;
    }

    /**
     * Menu permettant d'ajouter un client
     */
    @NotNull
    private JMenuItem menuItemAjouterClient() {
        @NotNull JMenuItem AjouterClient = new JMenuItem("Ajouter Client");
        AjouterClient.addActionListener(e -> new AjouterClient());
        return AjouterClient;
    }

    /**
     * Menu permettant de garer un vehicule
     */
    @NotNull
    private JMenuItem menuItemGarerVehicule() {
        @NotNull final JMenuItem AjouterVehicule = new JMenuItem("Garer un Vehicule");
        AjouterVehicule.addActionListener(e -> new GarerVehicule());
        return AjouterVehicule;
    }

    /**
     * Menu permettant d'enlever un vehicule
     */
    @NotNull
    private JMenuItem menuItemEnleverVehicule() {
        @NotNull final JMenuItem EnleverVehicule = new JMenuItem("Enlever un vehicule");
        EnleverVehicule.addActionListener(e -> new EnleverVehicule());
        return EnleverVehicule;
    }
}
