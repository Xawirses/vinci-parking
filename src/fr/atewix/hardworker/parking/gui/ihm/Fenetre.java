package fr.atewix.hardworker.parking.gui.ihm;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;

/**
 * Created by michael on 18/01/2015.
 */
public class Fenetre extends JFrame {

    protected Fenetre(String nom, @NotNull Dimension taille) {
        super(nom);
        setResizable(false);
        setPreferredSize(taille);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);
    }
}
