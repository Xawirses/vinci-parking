package fr.atewix.hardworker.parking.gui.ihm;

import fr.atewix.hardworker.parking.gui.*;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuParking extends JMenuBar implements ActionListener {

    @NotNull
    private static final String SHOW_PARKING = "Visualiser Etat du Parking";
    @NotNull
    private static final String QUIT = "Quitter";
    @NotNull
    private static final String CREER_CLIENT = "Ajouter Client";
    @NotNull
    private static final String CREER_VOITURE = "Ajouter Vehicule";
    @NotNull
    private static final String CREER_RESERVATION = "Ajouter Réservation";
    @NotNull
    private static final String DELETE_RESERVATION = "Enlever une Reservation";
    @NotNull
    private static final String GARER_VEHICULE = "Garer un Vehicule";
    @NotNull
    private static final String ENLEVER_VEHICULE = "Enlever un vehicule";

    public MenuParking() {
        super();
        add(menuFichier());
        add(menuEditer());
        add(menuClient());
    }

    @NotNull
    private JMenu menuFichier() {
        @NotNull JMenu Fichier = new JMenu("Fichier");
        Fichier.add(nouveauItem(SHOW_PARKING));
        Fichier.add(nouveauItem(QUIT));
        return Fichier;
    }

    @NotNull
    private JMenu menuEditer() {
        @NotNull JMenu Editer = new JMenu("Editer");
        Editer.add(nouveauItem(CREER_RESERVATION));
        Editer.add(nouveauItem(DELETE_RESERVATION));
        Editer.add(nouveauItem(GARER_VEHICULE));
        Editer.add(nouveauItem(ENLEVER_VEHICULE));
        return Editer;
    }

    @NotNull
    private JMenu menuClient() {
        @NotNull JMenu Client = new JMenu("Client");
        Client.add(nouveauItem(CREER_CLIENT));
        Client.add(nouveauItem(CREER_VOITURE));
        return Client;
    }

    @NotNull
    private JMenuItem nouveauItem(@NotNull String text) {
        @NotNull JMenuItem menuItem = new JMenuItem(text);
        menuItem.addActionListener(this);
        return menuItem;
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        switch (commande) {
            case SHOW_PARKING:
                new EtatParking();
                break;
            case QUIT:
                System.exit(0);
            case CREER_CLIENT:
                new AjouterClient();
                break;
            case CREER_VOITURE:
                new AjouterVehicule();
                break;
            case CREER_RESERVATION:
                new AjouterReservation();
                break;
            case GARER_VEHICULE:
                new GarerVehicule();
                break;
            case DELETE_RESERVATION:
                new EnleverReservation();
                break;
            case ENLEVER_VEHICULE:
                new EnleverVehicule();
                break;
            default:
                throw new IllegalStateException();
        }
    }
}
