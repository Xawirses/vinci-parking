package fr.atewix.hardworker.parking.gui.ihm;

import fr.atewix.hardworker.parking.gui.AjouterClient;
import fr.atewix.hardworker.parking.gui.AjouterReservation;
import fr.atewix.hardworker.parking.gui.AjouterVehicule;
import fr.atewix.hardworker.parking.gui.GarerVehicule;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by michael on 18/01/2015.
 */
public class MenuRapide extends JPanel implements ActionListener {

    @NotNull
    private static final String CREER_CLIENT = "Nouveau Client";
    @NotNull
    private static final String CREER_VOITURE = "Nouveau Véhicule";
    @NotNull
    private static final String CREER_RESERVATION = "Nouvelle Réservation";
    @NotNull
    private static final String GARER_VEHICULE = "Garer Véhicule";

    public MenuRapide() {
        super();
        add(newButon(CREER_CLIENT));
        add(newButon(CREER_VOITURE));
        add(newButon(CREER_RESERVATION));
        add(newButon(GARER_VEHICULE));
    }

    @NotNull
    private JButton newButon(@NotNull String text) {
        @NotNull JButton bouton = new JButton(text);
        bouton.setMargin(new Insets(0, 0, 0, 0));
        bouton.setPreferredSize(new Dimension(160, 25));
        bouton.addActionListener(this);
        return bouton;
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        switch (commande) {
            case CREER_CLIENT:
                new AjouterClient();
                break;
            case CREER_VOITURE:
                new AjouterVehicule();
                break;
            case CREER_RESERVATION:
                new AjouterReservation();
                break;
            case GARER_VEHICULE:
                new GarerVehicule();
                break;
            default:
                throw new IllegalStateException();
        }
    }
}
