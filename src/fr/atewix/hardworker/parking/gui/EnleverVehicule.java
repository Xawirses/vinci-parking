package fr.atewix.hardworker.parking.gui;


import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.exception.PlaceLibreException;
import fr.atewix.hardworker.parking.gui.ihm.Fenetre;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class EnleverVehicule extends Fenetre implements ActionListener {

    @NotNull
    private final Parking parking = Parking.getInstance();
    @NotNull
    private final JComboBox<String> lplace = new JComboBox<>();

    public EnleverVehicule() {
        super("EnleverVehicule", new Dimension(300, 150));
        add(MainPannel());
        setVisible(true);
    }

    @NotNull
    private JPanel ChoixPlacePannel() {
        @NotNull JPanel ChoixPlace = new JPanel();
        ChoixPlace.setLayout(new BorderLayout());
        @NotNull JLabel Labelplace = new JLabel("Place");
        for (int i = 0; i < parking.getNombrePlace(); ++i) {
            lplace.addItem("" + i);
        }
        ChoixPlace.add(Labelplace, BorderLayout.NORTH);
        ChoixPlace.add(lplace, BorderLayout.CENTER);
        return ChoixPlace;
    }

    @NotNull
    private JPanel ValiderAnnuller() {
        @NotNull JPanel ValiderAnnuler = new JPanel();
        @NotNull JButton Valider = new JButton();
        Valider.setText("Valider");
        Valider.setPreferredSize(new Dimension(140, 40));
        Valider.addActionListener(this);

        @NotNull JButton Annuler = new JButton();
        Annuler.setText("Annuler");
        Annuler.setPreferredSize(new Dimension(140, 40));
        Annuler.addActionListener(this);
        ValiderAnnuler.add(Valider, BorderLayout.WEST);
        ValiderAnnuler.add(Annuler, BorderLayout.EAST);
        return ValiderAnnuler;
    }

    @NotNull
    private JPanel MainPannel() {
        @NotNull JPanel main = new JPanel();
        main.setLayout(new BorderLayout());
        main.add(ChoixPlacePannel(), BorderLayout.NORTH);
        main.add(ValiderAnnuller(), BorderLayout.SOUTH);
        return main;
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        if (commande.equals("Valider")) {
            try {
                parking.unpark(Integer.parseInt((String) lplace.getSelectedItem()));
                new FactureView();
                AffichageParking.getInstance().mettreAJour();

            } catch (@NotNull NumberFormatException | PlaceLibreException ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }
        } else if (commande.equals("Annuler")) {
            // do nothing
        }
        dispose();
    }
}
