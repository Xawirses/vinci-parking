package fr.atewix.hardworker.parking.gui;

import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.business.Client;
import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.exception.DejaReserveAilleurs;
import fr.atewix.hardworker.parking.exception.DejasGarerAilleur;
import fr.atewix.hardworker.parking.exception.PlusAucunePlaceException;
import fr.atewix.hardworker.parking.gui.ihm.Fenetre;
import fr.atewix.hardworker.parking.place.Place;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AjouterReservation extends Fenetre implements ActionListener {

    @NotNull
    private final Parking parking = Parking.getInstance();
    @NotNull
    private final JPanel panel = new JPanel();
    @NotNull
    private final JComboBox<Vehicule> lvehicule = new JComboBox<>();
    @NotNull
    private final JComboBox<Client> lclient = new JComboBox<>();

    /**
     * Ajouter une reservation
     */
    public AjouterReservation() {
        super("Reserver une place", new Dimension(400, 160));
        generateVue();
        setVisible(true);
    }

    /**
     * Generer Vue
     */
    private void generateVue() {

        @NotNull JPanel top = new JPanel();
        @NotNull JPanel bot = new JPanel();

        @NotNull JLabel labelClient = new JLabel("Client");
        for (Client client : parking.getListeClient()) {
            lclient.addItem(client);
        }

        @NotNull JLabel labelvehicule = new JLabel("Vehicule");
        lvehicule.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                lvehicule.removeAllItems();
                @NotNull Client cliensouhaite = (Client) lclient.getSelectedItem();
                for (Vehicule vehicule : cliensouhaite.getListeVehiculeClient()) {
                    lvehicule.addItem(vehicule);
                }
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        top.add(labelClient);
        top.add(lclient);
        top.add(labelvehicule);
        top.add(lvehicule);


        @NotNull JButton Valider = new JButton();
        Valider.setText("Valider");
        Valider.setPreferredSize(new Dimension(140, 40));

        Valider.addActionListener(this);

        @NotNull JButton Annuler = new JButton();
        Annuler.setText("Annuler");
        Annuler.setPreferredSize(new Dimension(140, 40));
        Annuler.addActionListener(this);

        bot.add(Valider);
        bot.add(Annuler);

        panel.add(top);
        panel.add(bot);
        setContentPane(panel);

    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        if (commande.equals("Valider")) {
            try {
                if (parking.vehiculeExiste((Vehicule) lvehicule.getSelectedItem())) {
                    throw new DejasGarerAilleur();
                }
                @NotNull Place placereserve = parking.bookPlace((Vehicule) lvehicule.getSelectedItem());
                AffichageParking.getInstance().mettreAJour();

            } catch (@NotNull PlusAucunePlaceException | DejaReserveAilleurs | DejasGarerAilleur e1) {
                Utilitaires.afficheMessageErreurModal(this, e1);
            }
        } else if (commande.equals("Annuler")) {
            // do nothing
        }
        dispose();
    }
}
