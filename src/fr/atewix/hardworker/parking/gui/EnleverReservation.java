package fr.atewix.hardworker.parking.gui;

import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.business.Client;
import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.business.Reservation;
import fr.atewix.hardworker.parking.exception.PlaceDisponibleException;
import fr.atewix.hardworker.parking.gui.ihm.Fenetre;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class EnleverReservation extends Fenetre implements ActionListener {

    @NotNull
    private final Parking parking = Parking.getInstance();
    @NotNull
    private final JComboBox<Client> lclient = new JComboBox<>();
    @NotNull
    private final JComboBox<Vehicule> lvehicule = new JComboBox<>();
    @NotNull
    private final JComboBox<Reservation> lreservation = new JComboBox<>();

    public EnleverReservation() {
        super("Enlever Reservation", new Dimension(400, 200));
        @NotNull JPanel main = new JPanel();
        main.setLayout(new BorderLayout());
        main.add(HautPanel(), BorderLayout.NORTH);
        main.add(Center(), BorderLayout.CENTER);
        add(main);
        setVisible(true);
    }

    @NotNull
    private JPanel HautPanel() {
        @NotNull JPanel hautpanel = new JPanel();
        hautpanel.setLayout(new BorderLayout());
        hautpanel.add(ClientPanel(), BorderLayout.NORTH);
        hautpanel.add(VoiturePanel(), BorderLayout.CENTER);
        return hautpanel;
    }

    @NotNull
    private JPanel ClientPanel() {
        @NotNull JPanel client = new JPanel();
        client.setLayout(new BorderLayout());
        @NotNull JLabel labelClient = new JLabel("Client");
        for (Client c : parking.getListeClient()) {
            lclient.addItem(c);
        }
        lclient.setPreferredSize(new Dimension(300, 20));
        client.add(labelClient, BorderLayout.NORTH);
        client.add(lclient, BorderLayout.CENTER);
        return client;
    }

    @NotNull
    private JPanel Center() {
        @NotNull JPanel center = new JPanel();
        center.setLayout(new BorderLayout());
        center.add(ReservationPanel(), BorderLayout.NORTH);
        center.add(ValiderAnnuler(), BorderLayout.CENTER);
        return center;
    }

    @NotNull
    private JPanel VoiturePanel() {
        @NotNull JPanel vehicule = new JPanel();
        vehicule.setLayout(new BorderLayout());
        @NotNull JLabel labelvehicule = new JLabel("Vehicule");
        lvehicule.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                lvehicule.removeAllItems();
                @NotNull Client clientchoisi = (Client) lclient.getSelectedItem();
                for (Vehicule v : clientchoisi.getListeVehiculeClient()) {
                    lvehicule.addItem(v);
                }
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
        vehicule.add(labelvehicule, BorderLayout.NORTH);
        vehicule.add(lvehicule, BorderLayout.CENTER);
        return vehicule;
    }

    @NotNull
    private JPanel ReservationPanel() {
        @NotNull JPanel reservation = new JPanel();
        reservation.setLayout(new BorderLayout());
        @NotNull JLabel lreservationlabel = new JLabel("Reservation");
        lreservation.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                lreservation.removeAllItems();
                @NotNull Vehicule vehiculechoisi = (Vehicule) lvehicule.getSelectedItem();
                @Nullable Reservation reservation = parking.getReservationParVehicule(vehiculechoisi);
                lreservation.addItem(reservation);
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
        reservation.add(lreservationlabel, BorderLayout.NORTH);
        reservation.add(lreservation, BorderLayout.CENTER);
        return reservation;
    }

    @NotNull
    private JPanel ValiderAnnuler() {
        @NotNull JPanel validerannuler = new JPanel();
        @NotNull JButton Valider = new JButton();
        Valider.setText("Valider");
        Valider.setPreferredSize(new Dimension(140, 40));
        Valider.addActionListener(this);

        @NotNull JButton Annuler = new JButton();
        Annuler.setText("Annuler");
        Annuler.setPreferredSize(new Dimension(140, 40));
        Annuler.addActionListener(this);
        validerannuler.add(Valider, BorderLayout.EAST);
        validerannuler.add(Annuler, BorderLayout.WEST);
        return validerannuler;
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        if (commande.equals("Valider")) {
            @NotNull Reservation reservationselectionnee = (Reservation) lreservation.getSelectedItem();
            parking.enleveruneReservation(reservationselectionnee);
            int numPlace = reservationselectionnee.getPlace().getNumPlace();

            try {
                parking.freePlace(numPlace);
                AffichageParking.getInstance().mettreAJour();
            } catch (PlaceDisponibleException ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }
        } else if (commande.equals("Annuler")) {
            // do nothing
        }
        dispose();
    }
}
