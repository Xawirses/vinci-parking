package fr.atewix.hardworker.parking.gui;

import fr.atewix.hardworker.parking.business.Parking;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.io.*;

public class EtatParking extends JFrame {

    public EtatParking() {
        super("Etat du Parking");
        @NotNull JPanel panel = new JPanel();
        @NotNull JTextArea etatParking = new JTextArea();
        etatParking.setText(Parking.getInstance().etatParking());
        etatParking.setEditable(false);
        panel.add(etatParking);
        setContentPane(panel);
        add(imprimerEtat());
        pack();
        setResizable(false);
        setVisible(true);
    }

    @NotNull
    private JButton imprimerEtat() {
        @NotNull JButton imprimerEtat = new JButton("Exporter Etat du parking");
        imprimerEtat.addActionListener(e -> {
            @NotNull String filename = "etatParking.txt";
            try (@NotNull DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(new File(filename))))) {
                dos.writeBytes(Parking.getInstance().etatParking());

                @NotNull JFrame information = new JFrame();
                JOptionPane.showMessageDialog(information,
                        "Etat du parking exporter !",
                        "Information",
                        JOptionPane.INFORMATION_MESSAGE);
            } catch (IOException ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }

        });
        return imprimerEtat;
    }
}
