package fr.atewix.hardworker.parking.gui;

import fr.atewix.hardworker.parking.Vehicule.FabriqueVehicule;
import fr.atewix.hardworker.parking.Vehicule.FabriqueVehiculeImpl;
import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.Vehicule.VehiculeType;
import fr.atewix.hardworker.parking.business.Client;
import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.exception.DonneesNonValides;
import fr.atewix.hardworker.parking.exception.ImmatriculationDejaUtilise;
import fr.atewix.hardworker.parking.gui.ihm.Fenetre;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class AjouterVehicule extends Fenetre implements ActionListener {

    @NotNull
    private final Parking parking = Parking.getInstance();
    @NotNull
    private final JPanel main = new JPanel();
    @NotNull
    private final JTextField Immatriculation = new JTextField();
    @NotNull
    private final JTextField Modele = new JTextField();
    @NotNull
    private final JTextField Marque = new JTextField();
    @NotNull
    private final JComboBox<Client> lclient = new JComboBox<>();
    @NotNull
    private final JComboBox<VehiculeType> typeVehicule = new JComboBox<>();

    public AjouterVehicule() {
        super("Ajouter Vehicule", new Dimension(320, 250));
        generateVue();
        add(main);
        setVisible(true);
    }

    private void generateVue() {
        @NotNull BorderLayout borderLayout = new BorderLayout();
        setLayout(borderLayout);

        @NotNull JPanel top = new JPanel();
        @NotNull JPanel topClient = new JPanel();
        @NotNull JPanel topVehicule = new JPanel();
        @NotNull JPanel center = new JPanel();
        @NotNull JPanel topCenter = new JPanel();
        @NotNull JPanel midCenter = new JPanel();
        @NotNull JPanel midCenterRight = new JPanel();
        @NotNull JPanel midCenterLeft = new JPanel();
        @NotNull JPanel bottom = new JPanel();

        top.setLayout(new BorderLayout());
        topClient.setLayout(new BorderLayout());
        center.setLayout(new BorderLayout());
        topCenter.setLayout(new BorderLayout());
        midCenter.setLayout(new BorderLayout());
        midCenterLeft.setLayout(new BorderLayout());
        midCenterRight.setLayout(new BorderLayout());
        bottom.setLayout(new BorderLayout());


        @NotNull JLabel labelClient = new JLabel("Client");
        for (@NotNull Client client : parking.getListeClient()) {
            lclient.addItem(client);
        }
        lclient.setPreferredSize(new Dimension(300, 20));

        topClient.add(labelClient, BorderLayout.NORTH);
        topClient.add(lclient, BorderLayout.CENTER);

        @NotNull JLabel labelTypeVehicule = new JLabel("Type de véhicule");
        typeVehicule.addItem(VehiculeType.Voiture);
        typeVehicule.addItem(VehiculeType.Moto);
        typeVehicule.addItem(VehiculeType.Camion);
        typeVehicule.setPreferredSize(new Dimension(300, 20));

        topVehicule.setLayout(new BorderLayout());
        topVehicule.add(labelTypeVehicule, BorderLayout.NORTH);
        topVehicule.add(typeVehicule, BorderLayout.CENTER);

        top.add(topClient, BorderLayout.NORTH);
        top.add(topVehicule, BorderLayout.CENTER);

        @NotNull JLabel labelImmatriculation = new JLabel("Immatriculation");
        Immatriculation.setPreferredSize(new Dimension(300, 20));

        topCenter.add(labelImmatriculation, BorderLayout.NORTH);
        topCenter.add(Immatriculation, BorderLayout.CENTER);

        @NotNull JLabel labelMarque = new JLabel("Marque");
        Marque.setPreferredSize(new Dimension(140, 20));
        midCenterLeft.add(labelMarque, BorderLayout.NORTH);
        midCenterLeft.add(Marque, BorderLayout.CENTER);

        @NotNull JLabel labelModele = new JLabel("Modele");
        Modele.setPreferredSize(new Dimension(140, 20));
        midCenterRight.add(labelModele, BorderLayout.NORTH);
        midCenterRight.add(Modele, BorderLayout.CENTER);

        midCenter.add(midCenterLeft, BorderLayout.WEST);
        midCenter.add(midCenterRight, BorderLayout.EAST);

        center.add(topCenter, BorderLayout.NORTH);
        center.add(midCenter, BorderLayout.CENTER);

        @NotNull JButton Valider = new JButton();
        Valider.setText("Valider");
        Valider.setPreferredSize(new Dimension(140, 40));
        Valider.addActionListener(this);
        bottom.add(Valider, BorderLayout.WEST);

        @NotNull JButton Annuler = new JButton();
        Annuler.setText("Annuler");
        Annuler.setPreferredSize(new Dimension(140, 40));
        Annuler.addActionListener(this);
        bottom.add(Annuler, BorderLayout.EAST);

        main.add(top, BorderLayout.NORTH);
        main.add(center, BorderLayout.CENTER);
        main.add(bottom, BorderLayout.SOUTH);
    }

    private boolean verifierDonneeVehicule(@NotNull Vehicule vehicule) throws DonneesNonValides, ImmatriculationDejaUtilise {
        @NotNull String immatriculation = vehicule.getImmatriculation();
        @NotNull String modele = vehicule.getModele();
        @NotNull String marque = vehicule.getMarque();

        if (parking.isImmatriculationExiste(immatriculation)) {
            throw new ImmatriculationDejaUtilise();
        }
        if (!immatriculation.matches("[A-Z]{2}[0-9]{3}[A-Z]{2}")) {
            throw new DonneesNonValides("immatriculation");
        }
        if (!modele.matches("[a-zA-Z0-9]{2,10}")) {
            throw new DonneesNonValides("modele");
        }
        if (!marque.matches("[a-zA-Z_0-9]{2,10}")) {
            throw new DonneesNonValides("marque");
        }
        return true;
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        if (commande.equals("Valider")) {
            String immatriculation = Immatriculation.getText();
            String modele = Modele.getText();
            String marque = Marque.getText();
            @NotNull Client proprietaire = (Client) lclient.getSelectedItem();
            @NotNull VehiculeType type = (VehiculeType) typeVehicule.getSelectedItem();
            try {
                @NotNull FabriqueVehicule fabriqueVehicule = new FabriqueVehiculeImpl();
                @NotNull Vehicule vehiculeAajouter = fabriqueVehicule.Creer(type, immatriculation, proprietaire, marque, modele);
                if (verifierDonneeVehicule(vehiculeAajouter)) {
                    proprietaire.addVehicule(vehiculeAajouter);
                }
            } catch (@NotNull DonneesNonValides | ImmatriculationDejaUtilise ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }
        } else if (commande.equals("Annuler")) {
            // do nothing
        }
        dispose();
    }
}
