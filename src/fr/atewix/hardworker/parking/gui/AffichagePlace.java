package fr.atewix.hardworker.parking.gui;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Class AffichagePlace, qui represente une place
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see javax.swing.JButton
 * @see java.awt.event.ActionListener
 */
class AffichagePlace extends JButton implements ActionListener {

    /**
     * numero de la place
     */
    private final int numPlace;

    /**
     * Constructeur d'une place sous forme JButton
     */
    public AffichagePlace(int numPlace) {
        this.numPlace = numPlace;
        addActionListener(this);
    }

    public void actionPerformed(ActionEvent e) {
        @NotNull InfosPlace infosPlace = new InfosPlace(this.numPlace);
    }
}
