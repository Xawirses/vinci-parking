package fr.atewix.hardworker.parking.gui;

import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;

/**
 * @author Jean-Pierre PRUNARET
 * @date 2017-02-17
 */
class Utilitaires {

    static void afficheMessageErreurModal(Component parent, @NotNull Exception e) {
        if (!SwingUtilities.isEventDispatchThread()) {
            throw new RuntimeException("a swing component must be invoked from SWING context");
        }

        @NotNull final String message = e.getMessage();
        JOptionPane.showMessageDialog(parent, message, "Erreur", JOptionPane.ERROR_MESSAGE);
    }
}
