package fr.atewix.hardworker.parking.gui;


import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.business.Client;
import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.business.Reservation;
import fr.atewix.hardworker.parking.exception.DejasGarerAilleur;
import fr.atewix.hardworker.parking.exception.PlaceOccupeeException;
import fr.atewix.hardworker.parking.gui.ihm.Fenetre;
import fr.atewix.hardworker.parking.place.Place;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Classe GarreVehicule
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class GarerVehicule extends Fenetre implements ActionListener {

    @NotNull
    private final Parking parking = Parking.getInstance();
    @NotNull
    private final JPanel main = new JPanel();
    @NotNull
    private final JComboBox<Vehicule> lvehicule = new JComboBox<>();
    @NotNull
    private final JComboBox<Client> lclient = new JComboBox<>();
    @NotNull
    private final JComboBox<Reservation> lreservation = new JComboBox<>();
    @NotNull
    private final JComboBox<String> lplace = new JComboBox<>();

    public GarerVehicule() {
        super("Garer un vehicule", new Dimension(350, 300));
        generateVue();
        add(main);
        setVisible(true);
    }

    private void generateVue() {
        main.setLayout(new BorderLayout());
        main.add(HautPanel(), BorderLayout.NORTH);
        main.add(CenterPanel(), BorderLayout.CENTER);
    }

    @NotNull
    private JPanel HautPanel() {
        @NotNull JPanel haut = new JPanel();
        haut.setLayout(new BorderLayout());
        haut.add(ClientPanel(), BorderLayout.NORTH);
        haut.add(VehiculePanel(), BorderLayout.CENTER);
        haut.add(Reservation(), BorderLayout.SOUTH);
        return haut;
    }

    @NotNull
    private JPanel CenterPanel() {
        @NotNull JPanel center = new JPanel();
        center.setLayout(new BorderLayout());
        center.add(Place(), BorderLayout.NORTH);
        center.add(ValiderAnnuler(), BorderLayout.CENTER);
        return center;
    }

    @NotNull
    private JPanel ClientPanel() {
        @NotNull JPanel client = new JPanel();
        client.setLayout(new BorderLayout());
        @NotNull JLabel labelClient = new JLabel("Client");
        for (Client c : parking.getListeClient()) {
            lclient.addItem(c);
        }
        lclient.setPreferredSize(new Dimension(300, 20));
        client.add(labelClient, BorderLayout.NORTH);
        client.add(lclient, BorderLayout.CENTER);
        return client;
    }

    @NotNull
    private JPanel VehiculePanel() {
        @NotNull JPanel vehicule = new JPanel();
        vehicule.setLayout(new BorderLayout());
        @NotNull JLabel labelvehicule = new JLabel("Vehicule");
        lvehicule.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                lvehicule.removeAllItems();
                @NotNull Client clientchoisi = (Client) lclient.getSelectedItem();
                for (Vehicule v : clientchoisi.getListeVehiculeClient()) {
                    lvehicule.addItem(v);
                }
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
        vehicule.add(lvehicule, BorderLayout.CENTER);
        vehicule.add(labelvehicule, BorderLayout.NORTH);
        return vehicule;
    }

    @NotNull
    private JPanel Reservation() {
        @NotNull JPanel reservation = new JPanel();
        reservation.setLayout(new BorderLayout());
        @NotNull JLabel reservationlabel = new JLabel("Reservation");
        lreservation.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                lreservation.removeAllItems();
                @NotNull Vehicule vehiculechoisi = (Vehicule) lvehicule.getSelectedItem();
                @Nullable Reservation reservation = parking.getReservationParVehicule(vehiculechoisi);
                lreservation.addItem(reservation);
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
        reservation.add(reservationlabel, BorderLayout.NORTH);
        reservation.add(lreservation, BorderLayout.CENTER);
        return reservation;
    }

    @NotNull
    private JPanel Place() {
        @NotNull JPanel place = new JPanel();
        place.setLayout(new BorderLayout());
        @NotNull JLabel placeLabel = new JLabel("Place");
        for (int i = 0; i < parking.getNombrePlace(); ++i) {
            lplace.addItem("" + i);
        }
        place.add(placeLabel, BorderLayout.NORTH);
        place.add(lplace, BorderLayout.CENTER);
        return place;
    }

    @NotNull
    private JPanel ValiderAnnuler() {
        @NotNull JPanel validerannuler = new JPanel();
        validerannuler.setLayout(new BorderLayout());
        @NotNull JButton Valider = Valider();
        @NotNull JButton annuler = Annuler();
        validerannuler.add(Valider, BorderLayout.WEST);
        validerannuler.add(annuler, BorderLayout.EAST);
        @NotNull JPanel validerannuler1 = new JPanel();
        validerannuler1.add(validerannuler, BorderLayout.CENTER);
        return validerannuler1;
    }

    @NotNull
    private JButton Annuler() {
        @NotNull JButton annuler = new JButton();
        annuler.setText("Annuler");
        annuler.setPreferredSize(new Dimension(140, 40));
        annuler.addActionListener(this);
        return annuler;
    }

    @NotNull
    private JButton Valider() {
        @NotNull JButton valider = new JButton();
        valider.setText("Valider");
        valider.setPreferredSize(new Dimension(140, 40));
        valider.addActionListener(this);
        return valider;
    }

    @Override
    public void actionPerformed(@NotNull ActionEvent e) {
        String commande = e.getActionCommand();
        if (commande.equals("Valider")) {
            try {
                if (lreservation.getSelectedItem() != null) {
                    @NotNull Reservation reservationselectionne = (Reservation) lreservation.getSelectedItem();
                    @NotNull Place placereserve = reservationselectionne.getPlace();
                    parking.enleveruneReservation(reservationselectionne);
                    parking.park((Vehicule) lvehicule.getSelectedItem(), placereserve);
                    AffichageParking.getInstance().mettreAJour();

                } else {
                    parking.park((Vehicule) lvehicule.getSelectedItem(), Integer.parseInt((String) lplace.getSelectedItem()));
                    AffichageParking.getInstance().mettreAJour();
                }
            } catch (@NotNull PlaceOccupeeException | DejasGarerAilleur | NumberFormatException ex) {
                Utilitaires.afficheMessageErreurModal(this, ex);
            }
        } else if (commande.equals("Annuler")) {
            // do nothing
        }
        dispose();
    }
}
