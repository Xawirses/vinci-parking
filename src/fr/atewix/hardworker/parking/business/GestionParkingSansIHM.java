package fr.atewix.hardworker.parking.business;

import fr.atewix.hardworker.parking.Vehicule.FabriqueVehicule;
import fr.atewix.hardworker.parking.Vehicule.FabriqueVehiculeImpl;
import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.Vehicule.VehiculeType;
import org.jetbrains.annotations.NotNull;


class GestionParkingSansIHM {

    public static void main(String[] args) {

        @NotNull Parking parking = Parking.getInstance();
        @NotNull FabriqueVehicule fabriqueVehicule = new FabriqueVehiculeImpl();

        @NotNull Client client1 = new Client("Nom", "Prenom", "Adresse");

        parking.addClient(client1);

        @NotNull Vehicule V = fabriqueVehicule.Creer(VehiculeType.Voiture, "Voit1", client1, "Marquex", "Modelex");
        @NotNull Vehicule V2 = fabriqueVehicule.Creer(VehiculeType.Voiture, "Voit2", client1, "Marquey", "Modeley");

        client1.addVehicule(V);
    }
}

