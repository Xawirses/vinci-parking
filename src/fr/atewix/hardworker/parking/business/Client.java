package fr.atewix.hardworker.parking.business;

import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Class Client : permet de créer un objet client contenant toutes les informations propres à un client.
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class Client {

    /**
     * Prenom du client
     */
    @NotNull
    private final String prenom;

    /**
     * Nom du client
     */
    @NotNull
    private final String nom;

    /**
     * Adresse du client
     */
    @NotNull
    private final String adresse;

    /**
     * Liste des vehicules appartenant à ce client
     */
    @NotNull
    private final Collection<Vehicule> listeVehiculeClient;

    /**
     * Constructeur de la classe Client, qui crée un client à partir d'un prenom, d'un nom et d'une adresse
     */
    public Client(@NotNull String prenom, @NotNull String nom, @NotNull String adresse) {
        this.prenom = prenom;
        this.nom = nom;
        this.adresse = adresse;
        listeVehiculeClient = new ArrayList<>();
    }

    /**
     * Methode permettant de mettre dans une String toutes les informations utiles à un client
     */
    @NotNull
    public String toString() {
        return String.format("%s %s", nom, prenom);
    }

    /**
     * Methode permettant d'ajouter un vehicule à la liste des véhicules du client
     */
    public void addVehicule(@NotNull Vehicule vehicule) {
        listeVehiculeClient.add(vehicule);
    }

    /**
     * Methode permettant d'obtenir la liste des véhicules du client
     *
     * @return Liste des véhicules du client
     */
    @NotNull
    public Collection<Vehicule> getListeVehiculeClient() {
        return this.listeVehiculeClient;
    }

    /**
     * Methode permettant d'obtenir le prenom du client
     *
     * @return Prenom du client
     */
    @NotNull
    public String getPrenom() {
        return prenom;
    }

    /**
     * Methode permettant d'obtenir le nom du client
     *
     * @return Nom du client
     */
    @NotNull
    public String getNom() {
        return nom;
    }

    /**
     * Methode permettant d'obtenir l'adresse du client
     *
     * @return Adresse du client
     */
    @NotNull
    public String getAdresse() {
        return adresse;
    }
}
