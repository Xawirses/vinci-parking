package fr.atewix.hardworker.parking.business;

import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.Vehicule.VehiculeType;
import fr.atewix.hardworker.parking.exception.*;
import fr.atewix.hardworker.parking.facture.Facture;
import fr.atewix.hardworker.parking.place.Particulier;
import fr.atewix.hardworker.parking.place.Place;
import fr.atewix.hardworker.parking.place.PlaceType;
import fr.atewix.hardworker.parking.place.Transporteur;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

/**
 * Class Parking : Classe principale du programme, c'est la classe qui crée un objet Parking et qui possède toutes les méthodes
 * utiles à un Parking
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class Parking {

    /**
     * Variable constante du nombre de places de type Particulier
     */
    private static final int NOMBREDEPLACESPARTICULIER = 12;

    /**
     * Variable constante du nombre de places de type Transporteur
     */
    private static final int NOMBREDEPLACESTRANSPORTEUR = 6;

    /**
     * Variable constante du tarif horaire d'une place de parking
     */
    private static final int TARIFHORRAIRE = 2;

    /**
     * Instance du singleton de la classe parking
     */
    @NotNull
    private static Parking instance = new Parking();

    /**
     * Liste des places du Parking
     */
    @NotNull
    private final ArrayList<Place> listeDesPlaces = new ArrayList<>();

    /**
     * Liste des clients du parking
     */
    @NotNull
    private final Collection<Client> listeClient = new ArrayList<>();

    /**
     * Liste des reservations du parking
     */
    @NotNull
    private final Collection<Reservation> listeReservation = new ArrayList<>();

    /**
     * Pile representant la liste des factures
     */
    @NotNull
    private final Stack<Facture> listeFacture = new Stack<>();

    /**
     * Constructeur de la classe Parking
     */
    private Parking() {
        for (double i = 0; i < NOMBREDEPLACESPARTICULIER; ++i) {
            listeDesPlaces.add(new Particulier());
        }
        for (double i = 0; i < NOMBREDEPLACESTRANSPORTEUR; ++i) {
            listeDesPlaces.add(new Transporteur());
        }
    }

    /**
     * Methode permettant de récuperer l'instance de la classe Parking, ou dans crée une si elle n'existe pas
     *
     * @return Instance de la classe Parking
     */
    @NotNull
    public static Parking getInstance() {
        return instance;
    }

    /**
     * Methode permettant de garer à un vehicule donné à un place donnée
     * TODO: passer une place en argument à la place du numéro. Permet de virer le arraylist & éviter un IndexArrayOutOfBandException & NullPointerException
     */
    public void park(@NotNull Vehicule vehicule, int numPlace) throws PlaceOccupeeException, DejasGarerAilleur {
        Place emplacementPossible = this.getLocation(vehicule.getImmatriculation());
        if (emplacementPossible.estOccupee()) {
            throw new DejasGarerAilleur();
        }
        @Nullable Place placeSouhaite = listeDesPlaces.get(numPlace);
        if (placeSouhaite != null) {
            if (placeSouhaite.estLibre() && placeSouhaite.estReservee()) {
                if (vehicule.getType() == VehiculeType.Camion && placeSouhaite.getType() == PlaceType.Particulier) {
                    throw new PlaceOccupeeException();
                } else if (placeSouhaite.getType() == PlaceType.Transporteur && vehicule.getType() != VehiculeType.Camion) {
                    boolean placeTrouve = false;
                    for (int i = 0; i < listeDesPlaces.size(); ++i) {
                        Place place = listeDesPlaces.get(i);
                        if (place.getType() == PlaceType.Particulier && place.estLibre() && place.estSansReservation()) {
                            place.setVehiculeparke(vehicule);
                            listeDesPlaces.set(i, place);
                            placeTrouve = true;
                            break;
                        }
                    }
                    if (!placeTrouve) {
                        placeSouhaite.setVehiculeparke(vehicule);
                        listeDesPlaces.set(numPlace, placeSouhaite);
                    }
                } else {
                    placeSouhaite.setVehiculeparke(vehicule);
                    listeDesPlaces.set(numPlace, placeSouhaite);
                }
            } else if (placeSouhaite.estOccupee()) {
                throw new PlaceOccupeeException();
            }
        } else {
            throw new RuntimeException("La place demandée n'existe pas");
        }
    }

    /**
     * Methode permettant de garer un vehicule donné à une place donné. Cette méthode est utilisé uniquement
     * pour un vehicule qui a reservé une place.
     */
    public void park(@NotNull Vehicule vehicule, @NotNull Place place) {
        place.setVehiculeparke(vehicule);
    }

    /**
     * Methode qui retire un vehicule d'une place, dont on donne le numéro
     *
     * TODO: voir pourquoi la liberation de place ne fixe pas la date de sortie.
     * TODO: créer fonction pour libérer la place
     * @return Vehicule enlevé
     */
    @Nullable
    public Vehicule unpark(int numPlace) throws PlaceLibreException {
        @NotNull Place placeSouhaite = listeDesPlaces.get(numPlace);
        if (placeSouhaite.estLibre()) {
            throw new PlaceLibreException();
        } else {
            @Nullable Vehicule vehiculeparke = placeSouhaite.getVehiculeparke();
            @NotNull Facture facture = new Facture(vehiculeparke, placeSouhaite.getDateArrive(), TARIFHORRAIRE);
            listeFacture.push(facture);
            placeSouhaite.setVehiculeparke(null);
            listeDesPlaces.set(numPlace, placeSouhaite);
            placeSouhaite.enleverReservation();
            reorganiserPlaces(placeSouhaite);
            return vehiculeparke;
        }
    }

    /**
     * Methode qui retire un vehicule du parking à partir de sa plaque d'immatriculation
     *
     * @return Vehicule enlevé
     */
    @Nullable
    private Vehicule retirerVehicule(@NotNull String immatriculation) {
        @Nullable final Place place = this.getLocation(immatriculation);
        if (place == null) {
            return null;
        } else {
            @Nullable Vehicule vehiculearetirer = place.getVehiculeparke();
            place.setVehiculeparke(null);
            return vehiculearetirer;
        }
    }

    /**
     * Methode permettant de renvoyer dans une String l'état du parking
     * TODO: foreach
     * TODO: string builder
     *
     * @return Etat du parking dans une String
     */
    @NotNull
    public String etatParking() {
        @NotNull String etatParking = "";
        for (int i = 0; i < listeDesPlaces.size(); ++i) {
            Place place = listeDesPlaces.get(i);
            etatParking += "Numero de la place : " + i + "\n\r";
            etatParking += "Type de la place : " + place.getType() + "\n\r";
            if (place.getVehiculeparke() != null) {
                etatParking += "Informations sur le vehicule garé : " + place.getVehiculeparke() + "\n";
            } else if (place.getReservation() != null) {
                etatParking += "Cette place est reservé" + "\n\r";
            } else {
                etatParking += "Cette place est disponible" + "\n\r";
            }

            etatParking += "\n\r";
        }
        return etatParking;
    }

    /**
     * Methode permettant de savoir si un vehicule est garé sur une place du parking ou non
     *
     * @return True ou false en fonction si un vehicule existe ou non
     */
    public boolean vehiculeExiste(Vehicule vehicule) {
        for (@NotNull Place place : listeDesPlaces) {
            if (place.getVehiculeparke() == vehicule) {
                return true;
            }
        }
        return false;
    }

    /**
     * Methode permettant de reserver une place, pour un vehicule
     * TODO: expression trops longue
     */
    @NotNull
    public Place bookPlace(@NotNull Vehicule vehicule) throws PlusAucunePlaceException, DejaReserveAilleurs {
        if (aDejaReserve(vehicule)) {
            throw new DejaReserveAilleurs();
        } else {
            for (@NotNull Place place : listeDesPlaces) {
                if (place.getReservation() == null && place.getVehiculeparke() == null) {
                    if (place.getType() == PlaceType.Transporteur || (place.getType() == PlaceType.Particulier && vehicule.getType() != VehiculeType.Camion)) {
                        @NotNull Reservation reservation = new Reservation(vehicule, place);
                        place.reserver(reservation);
                        listeReservation.add(reservation);
                        return place;
                    }
                }
            }
            throw new PlusAucunePlaceException();
        }
    }

    /**
     * Methode permettant d'enlever une reservation pour le numéro d'une place donné
     */
    public void freePlace(int numPlace) throws PlaceDisponibleException {
        Place place = listeDesPlaces.get(numPlace);
        if (place.getReservation() == null) {
            throw new PlaceDisponibleException();
        } else {
            place.enleverReservation();
        }
    }

    /**
     * Methode permettant de recuperer la location d'un vehicule au sein du parking
     *
     * @return La place où est le vehicule, null sinon
     */
    @Nullable
    private Place getLocation(@NotNull String immatriculation) {
        for (int i = 0; i < listeDesPlaces.size(); ++i) {
            Place place = listeDesPlaces.get(i);
            @Nullable Vehicule vehicule = place.getVehiculeparke();
            if (vehicule != null && vehicule.getImmatriculation().contentEquals(immatriculation)) {
                return place;
            }
        }
        return null;
    }

    /**
     * Methode permettant d'ajouter un client à la liste des clients du parking
     */
    public void addClient(Client client) {
        this.listeClient.add(client);
    }

    /**
     * Methode permettant de réorganiser les place à chaques sorties d'un véhicule
     */
    private void reorganiserPlaces(@NotNull Place placeSouhaite) {
        for (@NotNull Place place : listeDesPlaces) {
            if (place.getType() == PlaceType.Transporteur) {
                if (place.getVehiculeparke() != null && (place.getVehiculeparke().getType() != VehiculeType.Camion)) {
                    @Nullable Vehicule vehicule = place.getVehiculeparke();
                    this.retirerVehicule(vehicule.getImmatriculation());
                    try {
                        this.park(vehicule, placeSouhaite.getNumPlace());
                        return;
                    } catch (@NotNull PlaceOccupeeException | DejasGarerAilleur e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    /**
     * Methode permettant de recuperer la liste des factures
     *
     * @return Liste des factures
     */
    @NotNull
    public Stack getListeFacture() {
        return this.listeFacture;
    }

    /**
     * Methode permettant de recuperer la liste des places du parking
     *
     * @return Liste des places du parking
     */
    @NotNull
    public ArrayList<Place> getListeDesPlaces() {
        return this.listeDesPlaces;
    }

    /**
     * Methode permettant de recuperer la liste des clients du parking
     *
     * @return Liste des clients du parking
     */
    @NotNull
    public Collection<Client> getListeClient() {
        return this.listeClient;
    }

    /**
     * Methode permettant de recuperer le nombres de places du parking
     */
    public int getNombrePlace() {
        return this.listeDesPlaces.size();
    }

    /**
     * Methode permettant de recuperer une reservation (si elle existe) pour un véhicule
     *
     * @return Reservation d'une place pour un vehicule (si elle existe), sinon null
     */
    @Nullable
    public Reservation getReservationParVehicule(Vehicule vehicule) {
        for (@NotNull Reservation reservation : listeReservation) {
            if (reservation.getVehicule() == vehicule) {
                return reservation;
            }
        }
        return null;
    }

    /**
     * Methode permettant d'enlever une reservation de la liste des reservations
     */
    public void enleveruneReservation(@NotNull Reservation reservation) {
        listeReservation.remove(reservation);
    }

    /**
     * Methode permettant de savoir si un vehicule a déjà reservé une place au même instant
     *
     * @return Vrai si il a déjà reservé, Faux sinon
     */
    private boolean aDejaReserve(@NotNull Vehicule vehicule) {
        return (getReservationParVehicule(vehicule) != null);
    }

    /**
     * Methode permettant de savoir si une immatriculation existe déjà parmi la liste des vehicules des clients
     *
     * @return Vrai si l'immatriculation existe déjà, Faux sinon
     */
    public boolean isImmatriculationExiste(@NotNull String immatriculation) {
        for (@NotNull Client client : listeClient) {
            for (@NotNull Vehicule vehicule : client.getListeVehiculeClient()) {
                if (vehicule.getImmatriculation().equals(immatriculation)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Methode permettant de savoir si une combinaisons Nom/Prenom existe déjà dans la liste des clients inscrits.
     *
     * @return Vrai si la combinaison Nom/Prenom existe déjà, Faux sinon
     */
    public boolean isNomPrenomExiste(@NotNull String nom, @NotNull String prenom) {
        for (@NotNull Client client : listeClient) {
            if (client.getNom().equals(nom) && client.getPrenom().equals(prenom)) {
                return true;
            }
        }
        return false;
    }
}
