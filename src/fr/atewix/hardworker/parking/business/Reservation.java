package fr.atewix.hardworker.parking.business;

import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.place.Place;
import org.jetbrains.annotations.NotNull;

/**
 * Class Reservation, qui est un objet representant une reservation avec l'ensemble des informations necéssaires à une reservation
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public class Reservation {

    /**
     * Vehicule concerné par la réservation
     */
    @NotNull
    private final Vehicule vehicule;

    /**
     * Immatriculation du vehicule concerné par la reservation
     */
    @NotNull
    private final String immatriculation;

    /**
     * Place concerné par la reservation
     */
    @NotNull
    private final Place place;

    /**
     * Constructeur de la classe reservation, qui crée une reservation à partir d'un vehicule et d'une place
     */
    public Reservation(@NotNull Vehicule vehicule, @NotNull Place place) {
        this.place = place;
        this.vehicule = vehicule;
        this.immatriculation = vehicule.getImmatriculation();
    }

    /**
     * Methode qui inclue dans une String toutes les informations de la reservation
     *
     * @return String ayant les informations de la reservation
     */
    @NotNull
    public String toString() {
        return String.format(" Immatriculation : %s - Place n° :%d\n", immatriculation, place.getNumPlace());
    }

    /**
     * Methode qui renvoi le vehicule concerné par la reservation
     *
     * @return Vehicule de la reservation
     */
    @NotNull
    public Vehicule getVehicule() {
        return this.vehicule;
    }

    /**
     * Methode qui accède à la variable Place de la reservation
     *
     * @return Place concerné par la reservation
     */
    @NotNull
    public Place getPlace() {
        return this.place;
    }
}
