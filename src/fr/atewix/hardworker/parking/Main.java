package fr.atewix.hardworker.parking;

import fr.atewix.hardworker.parking.Vehicule.FabriqueVehicule;
import fr.atewix.hardworker.parking.Vehicule.FabriqueVehiculeImpl;
import fr.atewix.hardworker.parking.business.Client;
import fr.atewix.hardworker.parking.business.Parking;
import fr.atewix.hardworker.parking.gui.AffichageParking;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

import static fr.atewix.hardworker.parking.Vehicule.VehiculeType.*;

/**
 * Class Main, qui représente la classe de départ d'application
 */
class Main {
    /**
     * Constructeur de la classe Main
     */
    private Main() {
        @NotNull final FabriqueVehicule fabriqueVehicule = new FabriqueVehiculeImpl();
        @NotNull final Parking parking = Parking.getInstance();
        @NotNull Client c1 = new Client("Label", "Tristan", "22 Rue Gaston Berger");
        @NotNull Client c2 = new Client("Dumas", "Sabine", "17 Rue D'Alpagage");
        @NotNull Client c3 = new Client("Portal", "Ursula", "3 Rue de la liberté");
        @NotNull Client c4 = new Client("Tabelo", "Jack", "25 avenue fosh");

        for (Client client : Arrays.asList(c1, c2, c3, c4)) {
            parking.addClient(client);
        }

        c1.addVehicule(fabriqueVehicule.Creer(Voiture, "AB123CD", c1, "Ferrari", "LaFerrari"));
        c2.addVehicule(fabriqueVehicule.Creer(Voiture, "CB123CD", c2, "Peugeot", "205"));
        c2.addVehicule(fabriqueVehicule.Creer(Moto, "EF123CD", c2, "Ducati", "205"));
        c3.addVehicule(fabriqueVehicule.Creer(Voiture, "EF523CD", c2, "Ford", "escort"));
        c4.addVehicule(fabriqueVehicule.Creer(Camion, "EF823CD", c2, "Mercedes", "ATI"));

        @NotNull AffichageParking instance = AffichageParking.getInstance();
    }

    /**
     * Methode main, qui reprensente la méthode appelé lors de l'execution de l'application
     */
    public static void main(String[] args) {
        @NotNull Main main = new Main();
    }
}
