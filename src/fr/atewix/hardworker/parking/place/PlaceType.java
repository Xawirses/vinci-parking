package fr.atewix.hardworker.parking.place;

/**
 * @author Jean-Pierre PRUNARET
 * @date 2017-02-17
 */
public enum PlaceType {
    Transporteur, Particulier
}
