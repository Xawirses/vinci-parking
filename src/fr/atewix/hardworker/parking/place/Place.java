package fr.atewix.hardworker.parking.place;

import fr.atewix.hardworker.parking.Vehicule.Vehicule;
import fr.atewix.hardworker.parking.business.Reservation;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Class abstraite Place, qui represente un objet Place
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 */
public abstract class Place {

    /**
     * Variable static, permettant de donner à chaques places un numero unique
     */
    private static int numPlaceCree = 0;
    /**
     * Numéro de la place
     */
    private final int numPlace;
    /**
     * Type de la place
     */
    @NotNull
    final PlaceType type;
    /**
     * Vehicule garé sur cette place
     */
    @Nullable
    private Vehicule vehiculeparke;
    /**
     * Reservation active sur cette place
     */
    @Nullable
    private Reservation reservation;
    /**
     * Date d'arrivé d'un vehicule sur cette place
     */
    @Nullable
    private Calendar datearrive;

    /**
     * Constructeur de la classe Place
     */
    Place(@NotNull PlaceType type) {
        this.numPlace = numPlaceCree++;
        this.type = type;
    }

    public static int getNumPlaceCree() {
        return numPlaceCree;
    }

    /**
     * Methode qui permet de reserver cette place
     */
    public void reserver(@NotNull Reservation reservation) {
        this.reservation = reservation;
    }

    /**
     * Methode qui permet d'enlever une reservation de cette place
     */
    public void enleverReservation() {
        this.reservation = null;
    }

    /**
     * Methode qui permet de recuperer la date d'arrivée d'un vehicule sur cette place
     *
     * @return Date d'arrivée d'un vehicule
     */
    @Nullable
    public Calendar getDateArrive() {
        return this.datearrive;
    }

    /**
     * Methode qui permet de mettre une date arrivé à cette place
     */
    private void setDateArrive(@NotNull Calendar datearrivee) {
        this.datearrive = datearrivee;
    }

    /**
     * Methode qui permet de recuperer le numero de place de cette place
     *
     * @return Numero de cette place
     */
    public int getNumPlace() {
        return this.numPlace;
    }

    /**
     * Methode qui permet de recuperer le type de cette place
     *
     * @return Type de cette place
     */
    @NotNull
    public PlaceType getType() {
        return type;
    }

    /**
     * Methode qui permet de recuperer le vehicule garé sur cette place
     *
     * @return Vehicule garé sur cette place
     */
    @Nullable
    public Vehicule getVehiculeparke() {
        return vehiculeparke;
    }

    /**
     * Methode qui permet de mettre un vehicule sur cette place
     */
    public void setVehiculeparke(@NotNull Vehicule vehiculeparke) {
        this.setDateArrive(new GregorianCalendar());
        this.vehiculeparke = vehiculeparke;
    }

    /**
     * Methode qui permet de recuperer la reservation active sur cette place
     *
     * @return Reservation active sur cette place
     */
    @Nullable
    public Reservation getReservation() {
        return this.reservation;
    }

    public boolean estLibre() {
        return vehiculeparke == null;
    }

    public boolean estOccupee() {
        return !estLibre();
    }

    public boolean estReservee() {
        return reservation != null;
    }

    public boolean estSansReservation() {
        return !estReservee();
    }
}
