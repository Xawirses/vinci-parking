package fr.atewix.hardworker.parking.place;


import org.jetbrains.annotations.NotNull;

/**
 * Class Transporteur, qui represente un objet Place, de type Transporteur.
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see fr.atewix.hardworker.parking.place.Place
 */
public class Transporteur extends Place {

    /**
     * Constructeur de la classe Transporteur
     */
    public Transporteur() {
        super(PlaceType.Transporteur);
    }

    /**
     * Methode toString() de la classe Particulier
     */
    @NotNull
    public String toString() {
        return "Transporteur";
    }
}
