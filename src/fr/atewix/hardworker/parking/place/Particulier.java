package fr.atewix.hardworker.parking.place;

import org.jetbrains.annotations.NotNull;

/**
 * Class Particulier, qui represente une place de type Particulier
 *
 * @author Lucas Debiasi, Micheal Gileta, Sylvain De Barros, Kevin Duglue
 * @see fr.atewix.hardworker.parking.place.Particulier
 */
public class Particulier extends Place {

    /**
     * Constructeur de la classe Particulier
     */
    public Particulier() {
        super(PlaceType.Particulier);
    }

    /**
     * Methode toString() de la classe Particulier
     *
     * @return String contenant les informations propres à la classe Particulier
     */
    @NotNull
    public String toString() {
        return "Particulier";
    }
}
